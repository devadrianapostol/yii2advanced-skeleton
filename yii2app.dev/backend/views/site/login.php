<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use kartik\social\FacebookPlugin;
use kartik\social\Module;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to login:</p>

    <div class="row">
        <div class="col-lg-5">

            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            <?php
                /* @var $social Module */
                $social = Yii::$app->getModule('social');
                $callback = Url::toRoute(['/site/validate-fb'], true); // or any absolute url you want to redirect

                echo $social->getFbLoginLink($callback, ['class'=>'btn btn-primary']);
            ?>
            <?php ActiveForm::end(); ?>
            <div class="panel panel-body">
                <div class="page-header">
                    <h1>Facebook API Test Results <small><a href="http://demos.krajee.com/social">yii2-social</a></small></h1>
                </div>

                <hr>
                <?= Html::a('« Return', ['/site/social', '#' => 'facebook-api-example'], ['class'=>'btn btn-success']) ?>
            </div>

        </div>
    </div>
</div>
