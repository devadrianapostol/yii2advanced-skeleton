<?php
namespace backend\controllers;
use kartik\social\Module;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','validate-fb'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionValidateFb()
    {
        /* @var $social Module */
        $social = Yii::$app->getModule('social');
        $fb = $social->getFb(); // gets facebook object based on module settings
        try {
            $helper = $fb->getRedirectLoginHelper();
            $accessToken = $helper->getAccessToken();
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // There was an error communicating with Graph
            return $this->render('validate-fb', [
                'out' => '<div class="alert alert-danger">' . $e->getMessage() . '</div>'
            ]);
        }
        if (isset($accessToken)) { // you got a valid facebook authorization token
            $response = $fb->get('/me?fields=id,name,email', $accessToken);

            return $this->render('validate-fb', [
                'out' => '<legend>Facebook User Details</legend>' . '<pre>' . print_r($response, true) . '</pre>'
            ]);
        } elseif ($helper->getError()) {
            // the user denied the request
            // You could log this data . . .
            return $this->render('validate-fb', [
                'out' => '<legend>Validation Log</legend><pre>' .
                    '<b>Error:</b>' . print_r($helper->getError(), true) .
                    '<b>Error Code:</b>' . print_r($helper->getErrorCode(), true) .
                    '<b>Error Reason:</b>' . print_r($helper->getErrorReason(), true) .
                    '<b>Error Description:</b>' . print_r($helper->getErrorDescription(), true) .
                    '</pre>'
            ]);
        }
        return $this->render('', [
            'out' => '<div class="alert alert-warning"><h4>Oops! Nothing much to process here.</h4></div>'
        ]);
    }


}
