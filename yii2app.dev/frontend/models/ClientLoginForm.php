<?php
/**
 * Created by PhpStorm.
 * User: ady
 * Date: 29.03.2016
 * Time: 04:28
 */

namespace frontend\models;


use common\models\ClientUser;
use common\models\LoginForm;

class ClientLoginForm extends LoginForm{

    private $_user;

    /**
     * Finds user by [[username]]
     *
     * @return ClientUser|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = ClientUser::findByUsername($this->username);
        }

        return $this->_user;
    }
}